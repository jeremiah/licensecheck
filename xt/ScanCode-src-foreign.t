use Test2::V0;

use lib 't/lib';
use Test2::Licensecheck::ScanCode;

plan 8;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/non-english/licenses)],
	'xt/ScanCode-src-foreign.todo'
);

done_testing;
