use Test2::V0;

use lib 't/lib';
use Test2::Licensecheck::ScanCode;

plan 4827;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/rules)],
	'xt/ScanCode-src-rules.todo'
);

done_testing;
