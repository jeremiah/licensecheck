use Test2::V0;

use lib 't/lib';
use Test2::Licensecheck::ScanCode;

plan 18;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/composites/licenses)],
	'xt/ScanCode-src-composites.todo'
);

done_testing;
