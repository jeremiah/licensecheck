use Test2::V0;

use lib 't/lib';
use Test2::Licensecheck::ScanCode;

plan 1192;

are_licensed_like_scancode(
	[qw(src/licensedcode/data/licenses)],
	'xt/ScanCode-src.todo'
);

done_testing;
